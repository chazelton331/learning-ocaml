# OCaml setup

Here are some setup instructions for a Mac:

### Install OPAM

```bash
brew install opam
```

From [Real World OCaml V2](https://dev.realworldocaml.org/install.html):

> The entire OPAM package database is held in the .opam directory in your home directory, including compiler installations. On Linux and Mac OS X, this will be the ~/.opam directory, which means you don't need administrative privileges to configure it. If you run into problems configuring OPAM, just delete the whole ~/.opam directory and start over.

### Install OPAM

Run the following command to configure `opam`

```bash
opam init
```

> When the init command finishes, you'll see some instructions about environment variables. OPAM never installs files into your system directories (which would require administrator privileges). Instead, it puts them into your home directory by default, and can output a set of shell commands which configures your shell with the right PATH variables so that packages will just work.

Look at all available versions:

```bash
opam switch
```

And switch to the latest, e.g. if 4.08.0 is the latest then:

```bash
opam switch 4.08.0
```

### Install Libraries

Use `opam` to install some great libraries:

```bash
opam install base core_kernel ounit qcheck
```

Also, install `utop`, an interactive shell:

```bash
opam install utop
```

Put the following in `~/.ocamlinit`:

Place the following in the file .ocamlinit in your home directory should contain something like:

```ocaml
#use "topfind";;
#require "base";;
open Base
```

Real World OCaml V2 also recommends the following libraries. Couldn't hurt to install these now, too:

```bash
opam install async yojson core_extended core_bench cohttp async_graphics cryptokit menhir
```

### VIM <3

Since I love VIM, here's the plugin I've been using:

```bash
Plugin 'rgrinberg/vim-ocaml'
```

Add that to the `~/.vimrc` and run `:BundleInstall`
