# OCaml

## Goals

* Setup for local development
* Learn the language basics
* Learn about some more advanced compiler features
* Build a webservice
  * Framework comparisions
  * PostgreSQL connection and querying
  * JSON formatting
  * Redis interactions
  * Deployment
* Explore the [Tezos project](https://gitlab.com/tezos/tezos.git)

## Setup

[Here](./001_SETUP.md) are some instructions.

## Learn the language

Start with Real World OCaml, V2 (dev edition online)

* Start with the "Easy" Exercism exercises
  * Acronym
  * ?
* Follow the [guided tour](https://dev.realworldocaml.org/guided-tour.html) for some language basics
* Read about [modules](https://ocaml.org/learn/tutorials/modules.html)
